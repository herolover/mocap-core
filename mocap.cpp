#include "mocap.h"

mocap::calibration_set filter_outliers(const mocap::calibration_set &points)
{
  return points;
}

arma::mat33 compute_normalization_matrix(const std::vector<arma::vec2> &points)
{
  arma::vec2 centroid = utils::mean(points, (arma::vec2)arma::zeros(2));

  std::vector<double> distances;
  for (auto &point: points)
    distances.push_back(arma::norm(point - centroid, 2));

  double scale = sqrt(2.0) / utils::RMS(distances);

  arma::mat33 N = arma::eye(3, 3);
  N(0, 0) = scale;
  N(1, 1) = scale;
  N(0, 2) = -centroid(0) * scale;
  N(1, 2) = -centroid(1) * scale;

  return N;
}

arma::mat33 compute_fundamental_matrix_using_points(const std::vector<arma::vec2> &points0,
                                                    const std::vector<arma::vec2> &points1)
{
  const std::size_t points_count = points0.size();

  arma::mat33 T0 = compute_normalization_matrix(points0);
  arma::mat33 T1 = compute_normalization_matrix(points1);

  std::vector<arma::vec3> normalized_points0;
  std::vector<arma::vec3> normalized_points1;
  for (std::size_t i = 0; i < points_count; ++i)
  {
    normalized_points0.push_back(T0 * utils::H(points0[i]));
    normalized_points1.push_back(T1 * utils::H(points1[i]));
  }

  arma::mat A(points_count, 9);
  for (std::size_t i = 0; i < points_count; ++i)
  {
    A(i, 0) = normalized_points0[i](0) * normalized_points1[i](0);
    A(i, 1) = normalized_points0[i](1) * normalized_points1[i](0);
    A(i, 2) = normalized_points1[i](0);
    A(i, 3) = normalized_points0[i](0) * normalized_points1[i](1);
    A(i, 4) = normalized_points0[i](1) * normalized_points1[i](1);
    A(i, 5) = normalized_points1[i](1);
    A(i, 6) = normalized_points0[i](0);
    A(i, 7) = normalized_points0[i](1);
    A(i, 8) = 1.0;
  }

  arma::mat U;
  arma::vec s;
  arma::mat V;
  arma::svd(U, s, V, A);
  arma::mat33 F = arma::reshape(V.col(8), 3, 3).t();

  // constraint enforcement
  arma::svd(U, s, V, F);
  s(2) = 0.0;
  F = U * arma::diagmat(s) * V.t();

  // denormalization
  F = T1.t() * F * T0;

  return F;
}

// points consist of projections a point on all cameras
arma::vec3 triangulate(const std::vector<arma::mat34> &P,
                       const std::vector<arma::vec3> &points)
{
  arma::mat joined_P = utils::join_cols(P);
  arma::mat R = joined_P.cols(0, 2);
  arma::mat t = joined_P.col(3);

  //     | R1 x1 0 0 0 |     | -t1 |
  //     | R2 0 x2 0 0 |     | -t2 |
  // A = | R3 0 0 x3 0 | b = | -t3 |
  //     | R4 0 0 0 x4 |     | -t4 |
  arma::mat A = arma::join_rows(R, utils::join_diag(points));
  arma::vec b = -t;

  //     |  x  |
  //     |  y  |
  //     |  z  |
  //     | -w1 |
  // c = | -w2 | w - are weights of projection
  //     | -w3 |
  //     | -w4 |
  arma::vec c = arma::pinv(A) * b;

  return c.rows(0, 2);
}

// relatively the first cam
arma::mat34 compute_projection_matrix(const std::vector<arma::vec2> &points0,
                                      const std::vector<arma::vec2> &points1)
{
  arma::mat33 F = compute_fundamental_matrix_using_points(points0, points1);

  // computation R and t
  arma::mat33 U;
  arma::vec3 s;
  arma::mat33 V;
  arma::svd(U, s, V, F);

  arma::mat33 W = arma::zeros(3, 3);
  W(0, 1) = -1;
  W(1, 0) =  1;
  W(2, 2) =  1;

  std::vector<arma::mat33> R({U * W     * V.t(),
                              U * W.t() * V.t()});

  std::vector<arma::mat34> Pa = {arma::join_rows(R[0],  U.col(2)) / arma::det(R[0]),
                                 arma::join_rows(R[0], -U.col(2)) / arma::det(R[0]),
                                 arma::join_rows(R[1],  U.col(2)) / arma::det(R[1]),
                                 arma::join_rows(R[1], -U.col(2)) / arma::det(R[1])};

  // find a true `P`
  // check Z coordinate for a positivity in a local coordinate system of every camera
  for (auto &P: Pa)
  {
    arma::vec3 X0 = triangulate({arma::eye(3, 4), P},
                                {utils::H(points0[0]), utils::H(points1[0])});
    arma::vec3 X1 = P * utils::H(X0);

    if (X0(2) > 0 && X1(2) > 0)
      return P;
  }

  throw std::runtime_error("`P` matrix is not found.");
}

std::vector<arma::mat34> pairwise_calibrate(const mocap::calibration_set &set)
{
  std::vector<arma::mat34> P;
  P.push_back(arma::eye(3, 4));

  const std::size_t number_of_cameras = set.size();
  for (std::size_t i = 1; i < number_of_cameras; ++i)
    P.push_back(compute_projection_matrix(set[0], set[i]));

  return P;
}

mocap::CameraSystem mocap::calibrate(const calibration_set &set)
{
  auto filtered_set = filter_outliers(set);

  std::vector<arma::mat34> P = pairwise_calibrate(filtered_set);

  mocap::CameraSystem camera_system({P, {}});
  return camera_system;
}
