#ifndef MOCAP_H
#define MOCAP_H

#include <armadillo>
#include <vector>
#include <unordered_map>

#include "utils.h"

namespace mocap
{
  using calibration_set = std::vector<std::vector<arma::vec2>>;

  struct CameraSystem
  {
    std::vector<arma::mat34> P;
    utils::table<arma::mat33> F;
  };

  CameraSystem calibrate(const calibration_set &set);

}

#endif // MOCAP_H
