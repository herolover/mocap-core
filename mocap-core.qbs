import qbs 1.0

Project {
    StaticLibrary {
        name: "mocap-core"

        Depends {
            name: "cpp"
        }

        files: [
            "mocap.h",
            "mocap.cpp",
            "utils.h"
        ]

        cpp.cxxFlags: [
            "-std=c++11"
        ]

        cpp.linkerFlags: [
            "-larmadillo"
        ]
    }
}
