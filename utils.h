#ifndef UTILS_H
#define UTILS_H

#include <armadillo>

namespace arma
{
  typedef mat::fixed<2, 3> mat23;
  typedef mat::fixed<3, 4> mat34;
}

namespace utils
{
  template <class T>
  using table = std::unordered_map<std::size_t, std::unordered_map<std::size_t, T>>;

  template<class T> inline
  T sqr(T x)
  {
    return x * x;
  }

  template<class Tc, class Tv>
  Tv mean(const Tc &container, Tv init)
  {
    Tv mean = init;
    for (auto &elem: container)
      mean += elem;

    return mean / container.size();
  }

  template<class Tc>
  double RMS(const Tc &container)
  {
    double rms = 0.0;
    for (auto &elem: container)
      rms += sqr(elem);
    rms /= container.size();

    return sqrt(rms);
  }

  template<class T>
  arma::mat join_cols(const std::vector<T> &v)
  {
    const std::size_t rows = v[0].n_rows;
    const std::size_t cols = v[0].n_cols;

    arma::mat result(rows * v.size(), cols);
    for (std::size_t i = 0; i < v.size(); ++i)
      result.rows(i*rows, (i+1)*rows-1) = v[i];

    return result;
  }

  template<class T>
  arma::mat join_diag(const std::vector<T> &v)
  {
    const std::size_t rows = v[0].n_rows;
    const std::size_t cols = v[0].n_cols;

    arma::mat result = arma::zeros(v.size() * rows, v.size() * cols);
    for (std::size_t i = 0; i < v.size(); ++i)
      result.submat(i*rows, i*cols, (i+1)*rows-1, (i+1)*cols-1) = v[i];

    return result;
  }

  inline
  arma::vec H(const arma::vec &v, double w=1.0)
  {
    arma::vec result = v;
    result.resize(v.n_elem+1);
    result(v.n_elem) = w;

    return result;
  }
}

template<class Tm, class Tv>
std::vector<Tv> operator*(const Tm &m, const std::vector<Tv> &v)
{
  std::vector<Tv> result;
  for (auto &elem: v)
    result.push_back(m * elem);

  return result;
}

#endif
